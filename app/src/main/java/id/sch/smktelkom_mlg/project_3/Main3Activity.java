package id.sch.smktelkom_mlg.project_3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {
    TextView txtNama, txtTahun, txtAlamat, txtTelepon, txtEmail;
    String get_nama, get_alamat, get_email, get_tahun, get_telepon;
    Button buttonExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        buttonExit = findViewById(R.id.buttonExit);
        txtNama = findViewById(R.id.txtNama);
        txtTahun = findViewById(R.id.txtTahun);
        txtAlamat = findViewById(R.id.txtAlamat);
        txtTelepon = findViewById(R.id.txtTelepon);
        txtEmail = findViewById(R.id.txtEmail);
        Bundle b = getIntent().getExtras();
        get_nama = b.getString("parse_nama");
        get_tahun = b.getString("parse_tahun");
        get_alamat = b.getString("parse_alamat");
        get_telepon = b.getString("parse_telepon");
        get_email = b.getString("parse_email");

        txtNama.setText("Nama Anda adalah\n\t" + get_nama);
        txtTahun.setText("Tahun lahir Anda yaitu\n\t" + get_tahun);
        txtAlamat.setText("Alamat Anda ada di\n\t" + get_alamat);
        txtTelepon.setText("Nomor Telepon anda adalah\n\t" + get_telepon);
        txtEmail.setText("Alamat Email anda yaitu\n\t" + get_email);

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
