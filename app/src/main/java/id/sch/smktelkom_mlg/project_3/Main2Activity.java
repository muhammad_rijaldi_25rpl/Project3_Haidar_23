package id.sch.smktelkom_mlg.project_3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Main2Activity extends AppCompatActivity {
    Button btnSubmit, btnHapus;
    EditText editNama, editTahun, editAlamat, editTelepon, editEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        btnSubmit = findViewById(R.id.btnSubmit);
        btnHapus = findViewById(R.id.btnHapus);
        editNama = findViewById(R.id.editNama);
        editTahun = findViewById(R.id.editTahun);
        editAlamat = findViewById(R.id.editAlamat);
        editTelepon = findViewById(R.id.editTelepon);
        editEmail = findViewById(R.id.editEmail);
        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editNama.setText("");
                editTahun.setText("");
                editAlamat.setText("");
                editTelepon.setText("");
                editEmail.setText("");
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = editNama.getText().toString();
                String tahun = editTahun.getText().toString();
                String alamat = editAlamat.getText().toString();
                String telepon = editTelepon.getText().toString();
                String email = editEmail.getText().toString();
                boolean valid = true;

                //Validasi Form Nama
                if (nama.isEmpty()) {
                    editNama.setError("Nama Belum Di Isi");
                    valid = false;
                } else if (nama.length() < 3) {
                    editNama.setError("Nama minimal 3 karekter");
                    valid = false;
                } else {
                    editNama.setError(null);
                    editTahun.requestFocus();
                }

                //Validasi Form Tahun
                if (tahun.isEmpty()) {
                    editTahun.setError("Tahun Belum Di Isi");
                    valid = false;
                } else if (tahun.length() != 4) {
                    editTahun.setError("Format Tahun bukan yyyy");
                    valid = false;
                } else {
                    editTahun.setError(null);
                    editAlamat.requestFocus();
                }

                //Validasi alamat
                if (alamat.isEmpty()) {
                    editAlamat.setError("Alamat Belum Di Isi");
                    valid = false;
                } else {
                    editAlamat.setError(null);
                    editTelepon.requestFocus();
                }

                //Validasi telepon
                if (telepon.isEmpty()) {
                    editTelepon.setError("Nomor Telepon Belum Di Isi");
                    valid = false;
                } else {
                    editTelepon.setError(null);
                    editEmail.requestFocus();
                }

                //Validasi email
                if (email.isEmpty()) {
                    editEmail.setError("Email Belum Di Isi");
                    valid = false;
                } else {
                    editEmail.setError(null);
                    btnSubmit.requestFocus();
                }

                if (valid == true) {
                    Intent i = null;
                    i = new Intent(Main2Activity.this, Main3Activity.class);
                    Bundle b = new Bundle();
                    b.putString("parse_nama", nama);
                    b.putString("parse_tahun", tahun);
                    b.putString("parse_alamat", alamat);
                    b.putString("parse_telepon", telepon);
                    b.putString("parse_email", email);
                    i.putExtras(b);
                    startActivity(i);
                    finish();
                }

            }
        });
    }
}